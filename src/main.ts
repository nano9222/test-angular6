import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

declare let cordova: any;

if (environment.production) {
  enableProdMode();
}

if (typeof window['cordova'] !== 'undefined') {
  document.addEventListener("deviceready", () => {
	 platformBrowserDynamic().bootstrapModule(AppModule).catch(err => console.log(err))
   cordova.InAppBrowser.open('https://www.google.com/');
  }, false);
} else {
  platformBrowserDynamic().bootstrapModule(AppModule).catch(err => console.log(err));
}
